import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SobreComponent } from './navegacao/sobre/sobre.component';
import { MenusComponent } from './navegacao/menus/menus.component';
import { FooterComponent } from './navegacao/footer/footer.component';
import { ContatoComponent } from './navegacao/contato/contato.component';
import { ProdutoComponent } from './navegacao/produto/lista-produto/produto.component';
import { HomeComponent } from './navegacao/home/home.component';
import { FeaturesComponent } from './navegacao/features/features.component';

@NgModule({
  declarations: [
    AppComponent,
    SobreComponent,
    MenusComponent,
    FooterComponent,
    ContatoComponent,
    ProdutoComponent,
    HomeComponent,
    FeaturesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
